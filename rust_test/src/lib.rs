struct AdaptiveQuadrature<F> {
	f: F,
	a: f64,
	b: f64,
	tau: f64,
}

impl AdaptiveQuadrature {
	pub fn new(f, a, b, tau) -> Self {
		AdaptiveQuadrature {
			f: f,
			a: a,
			b: b,
			tau: tau
		}
	}

	pub fn integrate(&self, degree: usize) where M: QuadratureNodeMethod -> {
		
		// TODO precompute QuadratureNodeMethod weights
		let formula = M::new(self);

		// TODO could multithread this
		let mut sum = 0;
		for i in 0..degree {
			let x = formula.get_x(i, degree);
			let w = formula.get_weight(i, degree);
			sum += w * self.f(x);
		}


	}
}

pub trait QuadratureMethod {
	fn new(AdaptiveQuadrature) -> Self;
	fn get_node(usize, usize) -> f64;
	fn get_weight(usize, usize) -> f64;
	// fn est_error()
}

struct NewtonCotesMethod {
	a: f64,
	b: f64
}

impl QuadratureMethod for NewtonCotesMethod {
	fn new(quad: AdaptiveQuadrature) -> Self {
		NewtonCotesMethod {
			a: quad.a as f64,
			b: quad.b as f64
		}
	}

	fn x_at_index(index: usize, degree: usize) -> f64 {
		self.a + ((index as f64) / (degree as f64)) * (self.b - self.a)
	}

	fn weight_at_index(index: usize, n: usize) -> f64 {

	}
}


struct LagrangeInterp {
	
	// P1(x) = Product(k = 1..n) (x - x_k)
	// P2(x_j) = Product(k = 1..n) (x_j - x_k)
	// P3(x_j) = Product(k = 1..n, k != j) (x_j - x_k)

	Product(k, m) (x[i] - x[m]) / (x[j] - x[m])

}


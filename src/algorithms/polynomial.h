#pragma once

#include <vector>
#include <algorithm>
#include <numeric>
#include <tuple>
#include <math.h>
#include <boost/iterator/zip_iterator.hpp>

namespace algorithms {

	template<typename T = double>
	struct Polynomial {

		Polynomial(const std::vector<T> & coeffs, const std::vector<size_t> & exp)
			: coeffs(coeffs), exponents(exp), degree(*std::max_element(exp.begin(), exp.end()))
		{}

		T evaluate(const T x) const {
			auto begin = boost::make_zip_iterator(boost::make_tuple(coeffs.begin(), exponents.begin()));
			auto end = boost::make_zip_iterator(boost::make_tuple(coeffs.end(), exponents.end()));
			return std::accumulate(begin, end, 0.0, [&](const T sum, const boost::tuple<T, size_t> & el) {
				T coeff;
				size_t ex;
				boost::tie(coeff, ex) = el;
				return sum + coeff * pow(x, ex);
			});
		}

		const std::vector<T> coeffs;
		const std::vector<size_t> exponents; 
		const T degree;
	};

}
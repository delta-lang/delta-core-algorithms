#pragma once

#include "guassq/guassq.h"

namespace algorithms {

	namespace integration {

		/**
		 * Adaptive Quadrature with Guassian Quadrature kernel
		 *
		 * See https://www.wikiwand.com/en/Adaptive_quadrature
		 * 
		 * TODO
		 *   - Eventually this should be improved to work with multiple kernel algorithms
		 */
		
		template<typename T = double>
		struct Quadrature {
			
			Quadrature(const Polynomial<T> poly, const T mu) 
				: kernel(guassq::GuassQuad<T>(poly.degree, mu)), poly(poly)
				{}

			decltype(auto) solveForInterval(const T a, const T b) {
				return kernel.integratePolyForInterval(poly, a, b);
			}

		private:
			guassq::GuassQuad<T> kernel;
			Polynomial<T> poly;
		};

	}
}
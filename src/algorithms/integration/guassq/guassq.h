#pragma once

#include <Eigen/Eigenvalues>
#include <Eigen/Core>
#include <boost/iterator/zip_iterator.hpp>
#include <numeric>
#include <tuple>
#include <utility>

#include <iostream>

namespace algorithms { 
namespace integration {
namespace guassq {

	template<typename T = double>
	using NodeWeightPair = std::pair<
		Eigen::Array<T, Eigen::Dynamic, 1>,
		Eigen::Array<T, Eigen::Dynamic, 1>
	>;

	namespace detail {

		template<typename T = double>
		static decltype(auto)
		computeMatrixDiagonalsForDegree(size_t n) 
		{
			typedef Eigen::Array<T, Eigen::Dynamic, 1> RangeType;
			
			RangeType diag(n - 1, 1);
			diag << RangeType::LinSpaced(n - 1, 1, n - 1);
			diag *= 2;
			diag = 0.5 / sqrt(1 - diag.pow(-2));
			return diag;
		}

		template<typename T = double, typename A>
		static decltype(auto)
		createMatrixFromDiagonals(const A & diag) 
		{
			const size_t n = diag.size() + 1;
			Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> m(n, n);
			m.setZero();
			m.diagonal(1) = m.diagonal(-1) = diag;
			return m;
		}

		template<typename T = double, typename M>
		static NodeWeightPair<T>
		getNodeWeightsFromMatrix(const M & mat, const T mu)
		{
			Eigen::EigenSolver<M> eigs(mat);
			auto eigvecs = eigs.eigenvectors();
			const auto x = eigs.eigenvalues().real().array();
			auto w = eigvecs.row(0).real().array();
			w = mu * w.pow(2);
			return std::make_pair(w, x);
		}

		template<typename T = double>
		static NodeWeightPair<T>
		computeNodeWeightsForDegree(const size_t degree, const T mu) {
			auto diags = computeMatrixDiagonalsForDegree(degree);
			auto m = createMatrixFromDiagonals(diags);
			return getNodeWeightsFromMatrix(m, mu);
		}

	}

	template<typename T = double>
	struct GuassQuad {

		GuassQuad(const size_t degree, const T mu) 
			: wx(detail::computeNodeWeightsForDegree(degree, mu))
			{}

		template<typename W, typename X>
		static T integratePolyFromNegativeOneToOne(const Polynomial<T> & poly, const W & w, const X & x) {
			auto begin = boost::make_zip_iterator(boost::make_tuple(w.begin(), x.begin()));
			auto end = boost::make_zip_iterator(boost::make_tuple(w.end(), x.end()));
			return std::accumulate(begin, end, 0.0, [&](const T sum, const boost::tuple<T, T> & el) {
				T w, x;
				boost::tie(w, x) = el;
				return sum + w * poly.evaluate(x);
			});
		}

		T integratePolyForInterval(const Polynomial<T> & poly, const T a, const T b) {
			std::vector<T> w(wx.first.data(), wx.first.data() + wx.first.rows());
			std::vector<T> x(wx.second.data(), wx.second.data() + wx.second.rows());
			if (a == -1 && b == 1) {
				return integratePolyFromNegativeOneToOne(poly, w, x);
			}
			const auto c1 = (b - a) * 0.5;
			const auto c2 = (a + b) * 0.5;
			std::transform(x.begin(), x.end(), x.begin(), [&](const T x_i) {
				return c1 * x_i + c2;
			});
			return c2 * integratePolyFromNegativeOneToOne(poly, w, x);
		}

	private:
		NodeWeightPair<T> wx;
	};

}}}
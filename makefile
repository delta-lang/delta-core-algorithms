NAME = gauss_quad
SRC = ./src
TEST = ./test
INC = ./include
BUILD = ./target/debug
SYS_INC = /usr/local/include
SYS_LIB = /usr/local/lib
FRAMEWORKS = /System/Library/Frameworks

# Boost
BOOST_LIB = $(SYS_LIB)
BOOST_INC = $(SYS_INC)/boost
BOOST = -L$(BOOST_LIB) -I$(BOOST_INC)

# Eigen
EIGEN_INC = $(SYS_INC)/eigen3
EIGEN = -I$(EIGEN_INC)

CC = g++
CFLAGS = -std=c++14 -Wall
CFLAGS += -march=native -msse2 -msse4.2
C_SO_FLAGS = -fPIC -shared
C_RELEASE_FLAGS = -O3 -Os
CSRC = $(SRC)/api.cpp
C_TEST_SRC = $(TEST)/test.cpp
CLIBS = $(BOOST) $(EIGEN)
CINC = -I$(INC) -I./lib
CBUILD = $(BUILD)/lib$(NAME).so

all: .so

.so:
	$(CC) $(CFLAGS) $(C_SO_FLAGS) $(C_RELEASE_FLAGS) $(CLIBS) $(CINC) -o $(CBUILD) $(CSRC)

install:
	@cp $(BUILD)/* /usr/local/lib

clean:
	rm -rf $(BUILD)/*

.PHONY: all test clean

test:
	$(CC) $(CFLAGS) $(C_RELEASE_FLAGS) $(CLIBS) $(CINC) $(C_TEST_SRC)
#include "../src/algorithms/allheaders.h"
#include <assert.h>
#include <math.h>
#include <limits>
#include <iostream>

constexpr const double MACHINE_EPSILON = std::numeric_limits<double>::epsilon();
constexpr const double EPSILON = MACHINE_EPSILON * 100;


void test_polynomial();
void test_quadrature();


int main() {
	test_polynomial();	
	test_quadrature();
	return 0;
}

/* 
	evaluate the polynomial f(x) = 3x^2 + 2x^1 + 1 
*/
void test_polynomial() {
	const auto poly = algorithms::Polynomial<double>({3, 2, 1}, {2, 1, 0});
	assert(poly.evaluate(1) == 6);
	assert(poly.evaluate(3) == 34);
}

/*
	integrate f(x) = 2x + 1 from -1 to 1
*/
void test_quadrature_1() {
	const auto poly = algorithms::Polynomial<double>({2, 1}, {1, 0});
	assert(poly.evaluate(1) == 3);

	const double a = -1;
	const double b = 1;
	const double mu = 2;
	auto quad = algorithms::integration::Quadrature<double>(poly, mu);
	auto ans = quad.solveForInterval(a, b);
	assert(ans == 2);
}

/*
	integrate f(x) = 2x^3 + 5 from -1 to 1
*/
void test_quadrature_2() {
	const auto poly = algorithms::Polynomial<double>({2, 5}, {3, 0});
	assert(poly.evaluate(1) == 7);
	assert(poly.evaluate(3) == 59);

	const double a = -1;
	const double b = 1;
	const double mu = 2;
	auto quad = algorithms::integration::Quadrature<double>(poly, mu);
	auto ans = quad.solveForInterval(a, b);
	assert(ans == 10);
}

/*
	integrate f(x) = 2x^3 + 5 from 0 to 3
*/
void test_quadrature_3() {
	const auto poly = algorithms::Polynomial<double>({2, 5}, {3, 0});
	assert(poly.evaluate(1) == 7);
	assert(poly.evaluate(3) == 59);

	const double a = 0;
	const double b = 3;
	const double mu = 2;
	auto quad = algorithms::integration::Quadrature<double>(poly, mu);
	auto ans = quad.solveForInterval(a, b);
	assert(std::abs(ans - 55.5) < EPSILON);
}

void test_quadrature() {
	test_quadrature_1();
	test_quadrature_2();
	test_quadrature_3();
}